/// Created By: Vandana Gaikwad
/// Created Date: 12-April-2018

"use strict";
//Starting Solution
console.log("Loading Lambda...");

//Loading all necessary node modules.
// var aws = require("aws-sdk");
var promise = require("bluebird");
// var s3 = new aws.S3();

// var bucket = process.env.BALANCE_RECORDS_BUCKET;

// Lambda Function Started
exports.handler = function (event, context, callback) {
    var errorResponse = {
        errorMessage: ""
    }
    if (event && event.length) {
        console.log("Event is:" + JSON.stringify(event));
        exports.insertIntoS3(event)
            .then(function () {
                callback(null, event);
            })
            .catch(function (err) {
                console.log("Error:" + err);
                errorResponse.errorMessage = err;
                callback(errorResponse, null);
            })
    } else {
        console.log("No event Present");
        errorResponse.errorMessage = "No event Present";
        callback(errorResponse, null);
    }
};

//Binds data as per Athena Defination
exports.insertIntoS3 = (event) => {
    return new promise(function (resolve, reject) {
        console.log("Insert into S3 function invoked");
        if (event) {
            console.log(event);
            resolve("Success");
        } else {
            reject("error");
        }

    })
}