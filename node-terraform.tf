#provider
provider "aws" {
	
  access_key = "AKIAJNQUL6MOABOLOJ5A"
  secret_key = "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"
  region = "eu-west-1"
}

variable "core_region"      {
  default = "eu-west-1"
}
variable "lambda_execution" {
  default = "arn:aws:iam::620702670119:role/lambda_execution"
}
variable "iam_for_sfn"      {default = "arn:aws:iam::620702670119:role/AWSAdministratorAccess"}
variable "env"              {default = "dev"}
variable "role_arn" { default = "" }

# Create get-emp-details lambda
resource "aws_lambda_function" "get-employee-details" {
  function_name    = "${format("demo-%s-get-employee-details", "${var.env}")}"
  handler          = "app.handler"
  role             = "${var.lambda_execution}"
  runtime          = "nodejs6.10"
  filename         = "code.zip"
  source_code_hash = "${base64sha256(file("code.zip"))}"
  timeout          = 300
}

# Create DynamoDB table 
resource "aws_dynamodb_table" "basic-dynamodb-table" {
  name           = "${format("demo-%s-employee-details", "${var.env}")}"
  read_capacity  = 5
  write_capacity = 5
  hash_key       = "EmployeeId"
  range_key      = "EmployeeName"

  attribute {
    name = "EmployeeId"
    type = "S"
  }

  attribute {
    name = "EmployeeName"
    type = "S"
  }

  attribute {
    name = "TopScore"
    type = "N"
  }

  ttl {
    attribute_name = "TimeToExist"
    enabled = false
  }

  global_secondary_index {
    name               = "EmployeeIdIndex"
    hash_key           = "EmployeeId"
    range_key          = "TopScore"
    write_capacity     = 10
    read_capacity      = 10
    projection_type    = "INCLUDE"
    non_key_attributes = ["EmployeeName"]
  }
}

#s3 bucket for Employee records
resource "aws_s3_bucket" "store-employee-records" {
  bucket = "${format("employee-records-%s", "${var.core_region}")}"
  acl    = "private"
}

# kinesis stream storing employee records
resource "aws_kinesis_stream" "employee_operations_stream" {
  name        = "${format("employee-records-%s", "${var.env}")}"
  shard_count = "1"
}